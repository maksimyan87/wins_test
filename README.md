# Win-test

# Quickstart
1. Clone the repository
2. Activate the virtual environment
3. Put the images from https://www.kaggle.com/c/airbus-ship-detection/data to `data/images` directory
4. Run `python src/train.py`
5. Run `python src/test.py`
